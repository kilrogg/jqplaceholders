# jqPlaceholders

Replaces HTML5 placeholders with something nicer and makes them compatible even to older browsers.
Uses the standard HTML5 placeholder attributes as source
and adds new labels to display placeholder texts (customizable html).

(c) 2012 Benjamin Richter (b.richter@muenster.de)

Released under The MIT License.


## Usage:

1. Copy the contents of src/css and src/js to the corresponding asset directories in your project.

2. Insert the neccesary elements in your document's `<head>` section, e.g.:

        <link rel="stylesheet" href="css/jqp.css">
        <script src="js/jqp.js"></script>

 Remember to include jqp.js *after* including jQuery.

3. Initialise jqPlaceholders in your document.onload, e.g.:

        <script type="text/javascript">
			$(document).ready(function(){
				$('input').jqPlaceholders();
			});
		</script>


## Configuration:

	defaults = {
        containerHTML: '<span class="jqp">',
        labelHTML: '<label id="{label_id}" class="jqp" for="{element_id}">{placeholder_text}</label>',
        labelHide: true,
        labelCopyLayout: ['color', 'font-size', 'width', 'margin-top', 'padding-top', 'margin-left', 'padding-left'],
        animationOnHide: {'opacity': 0.0},
        animationOnFocus: {'opacity': 0.5},
        animationOnShow: {'opacity': 1.0},
        animationDuration: 250,
    };

**containerHTML** (string) is the html template for the surrounding container element (the one that wraps your selected input-Element).

**labelHTML** (string) is the label html template for the label element that's created:
 - {placeholder_text} is replaced by the placeholder attribute value of your input (or textarea, ...) element
 - {label_id} is replaced by the id of the created label !THIS IS REQUIRED FOR PROPER FUNCTION OF THE SCRIPT!
 - {element_id} is the id value of your input element

**labelHide** (boolean, default: true) controls if the label is hidden (display: none) after text is entered in the form field

**labelCopyLayout** (list, default: ['color', 'font-size', 'width', 'margin-top', 'padding-top', 'margin-left', 'padding-left']) controls, which properties of the input field are copied over to the placeholder

**animationOnHide, animationOnFocus, animationOnShow** are parameters to the jQuery *animate()*-function to control visuals when showing/hiding/focussing the form field

**animationDuration** (integer, default: 250) controls the animation duration (milliseconds) for all the animations


## Examples:

Examples can be found in the examples directory.

 /*!
 * jqPlaceholders
 * version: 1.0.0 (November 2012)
 * @requires jQuery
 * License: Licensed under the MIT license
 *
 * Copyright 2012 Benjamin Richter - b.richter@muenster.de
 *
 * ~based on jQuery lightweight plugin boilerplate by ajpiano~
 */
;
(function($, window, document, undefined) {

  // Create the defaults once
  var pluginName = 'jqPlaceholders',
    defaults = {
        containerHTML: '<span class="jqp">',
        labelHTML: '<label id="{label_id}" class="jqp" for="{element_id}">{placeholder_text}</label>',
        labelHide: true,
        labelCopyLayout: ['color', 'font-size', 'width', 'margin-top', 'padding-top', 'margin-left', 'padding-left'],
        animationOnHide: {'opacity': 0.0},
        animationOnFocus: {'opacity': 0.5},
        animationOnShow: {'opacity': 1.0},
        animationDuration: 250,
    };

  // The actual plugin constructor
  function Plugin(element, options) {
    this.element = element;

    this.options = $.extend({}, defaults, options);

    this._defaults = defaults;
    this._name = pluginName;

    this.init();
  }

  Plugin.prototype = {

    init: function() {
        if (!this.label) {
            this.element = $(this.element);
            this.setupHTML();
        }

        return this;
	},

	setupHTML: function() {
		var placeholder_text = this.element.attr('placeholder');
		var element_id = this.element.attr('id');

		if (!element_id) {
			element_id = 'id_' + this.element.attr('name');
			this.element.attr('id', element_id);
		}

		var label_id = 'jqp_' + element_id;

		this.element.removeAttr('placeholder');

		// add a span around our element
		this.element.wrap(this.options.containerHTML);
		// add a label after our element
		this.element.after(this.options.labelHTML.replace('{label_id}', label_id).replace('{element_id}', element_id).replace('{placeholder_text}', placeholder_text));

		this.label = $("#" + label_id);

		for (var i in this.options.labelCopyLayout) {
			var option = this.options.labelCopyLayout[i];
			this.label.css(option, this.element.css(option));
		}

		if (this.element.val() !== '') {
			this.label.css(this.options.animationOnHide);
			this.options.labelHide && this.label.hide();
		}

		this.element.focus(this.inputFocussed);
		this.element.blur(this.inputBlurred);
		this.element.bind('keydown', this.textEntered);
	},

    inputFocussed: function() {
		var $this = $(this);
		var plugin = $this.data('plugin_' + pluginName);
		var label = plugin.label;
		var options = plugin.options;

		if ($this.val() !== '') {
			label.stop().animate(options.animationOnHide, options.animationDuration, function() { options.labelHide && label.hide(); });
		} else {
			label.stop().animate(options.animationOnFocus, options.animationDuration);
		}
    },

	textEntered: function(e) {
		var $this = $(this);
		var plugin = $this.data('plugin_' + pluginName);
		var label = plugin.label;
		var options = plugin.options;

		if (
			(e.keyCode == 16) || // Skip Shift
			(e.keyCode == 9) // Skip Tab
		) return;

		label.stop().animate(options.animationOnHide, options.animationDuration, function() {  options.labelHide && label.hide(); });
	},

	inputBlurred: function() {
		var $this = $(this);
		var plugin = $this.data('plugin_' + pluginName);
		var label = plugin.label;
		var options = plugin.options;

		if ($this.val() === '') {
			label.show();
			label.stop().animate(options.animationOnShow, options.animationDuration);
		}

	}
  };

  $.fn[pluginName] = function(options) {
    var args = arguments;
    if (options === undefined || typeof options === 'object') {
      return this.each(function() {
        if (!$.data(this, 'plugin_' + pluginName)) {
          $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
        }
      });
    } else if (typeof options === 'string' && options[0] !== '_' && options !== 'init') {
      return this.each(function() {
        var instance = $.data(this, 'plugin_' + pluginName);
        if (instance instanceof Plugin && typeof instance[options] === 'function') {
          instance[options].apply(instance, Array.prototype.slice.call(args, 1));
        }
      });
    }
  };

})(jQuery, window, document);
